package org.sresingam.sip;
import java.io.*;
import java.util.LinkedList;
import java.util.Scanner;

import okhttp3.*;
import org.sresingam.sip.input.TestCase;
import org.sresingam.sip.input.impl.XmlConversionTestCaseReader;
import org.sresingam.sip.input.impl.XmlMathTestCaseReader;
import org.sresingam.sip.pojo.ConversionTestCaseObject;
import org.sresingam.sip.pojo.MathTestCaseObject;
public class TestURL {
	String host = "http://localhost:8081"; //default host
	OkHttpClient client = new OkHttpClient().newBuilder().build();
	
	public TestURL(String host) {
		this.host = host;
	}
	
	public TestURL() {
		
	}
	
	public String conversionTest(String url, String value, String from, String to){
			HttpUrl.Builder urlBuilder = HttpUrl.parse(url).newBuilder();
			urlBuilder.addQueryParameter("value", value);
			urlBuilder.addQueryParameter("from", from);
			urlBuilder.addQueryParameter("to", to);
			String address = urlBuilder.build().toString();
			return address;
	}
	
	public String lengthTest(String value, String from, String to) {
		String url = host+"/conversion/api/length?";
		return conversionTest(url,value,from,to);
	}
	
	public String weightTest(String value, String from, String to) {
		String url = host+"/conversion/api/weight?";
		return conversionTest(url,value,from,to);
	}
	
	public String mathTest(String url, String input, String constant){
		HttpUrl.Builder urlBuilder = HttpUrl.parse(url).newBuilder();
		urlBuilder.addQueryParameter("input", input);
		urlBuilder.addQueryParameter("constant", constant);
		String address = urlBuilder.build().toString();
		return address;
	}
	
	public String divideTest(String input, String constant) {
		String url = host+"/math/api/divide?";
		return mathTest(url,input,constant);
	}
	public String minusTest(String input, String constant) {
		String url = host+"/math/api/minus?";
		return mathTest(url,input,constant);
	}
	public String multipleTest(String input, String constant) {
		String url = host+"/math/api/multiple?";
		return mathTest(url,input,constant);
	}
	public String plusTest(String input, String constant) {
		String url = host+"/math/api/plus?";
		return mathTest(url,input,constant);
	}
	
	public String doGetRequest(String url) throws IOException {
		Request request = new Request.Builder().url(url).build();
		Response response = client.newCall(request).execute();
		return response.body().string();
	}

	public static String[] ReadFile(String filename) {
		String data[] = new String[3];
		int i = 0;
		try {
			File myObj = new File(filename);
			Scanner myReader = new Scanner(myObj);
			while (myReader.hasNextLine()) {
				data[i] = myReader.nextLine();
				i++;
			}
		    myReader.close();
		} catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		    e.printStackTrace();
		  }
		return data;
	}
	
	public void executeConversionTests(TestCase conversionTestCaseReader) {
		LinkedList<ConversionTestCaseObject> conversionTestCases = conversionTestCaseReader.getTestCaseObjectList();

		System.out.println("===================================");
		System.out.println("Testing "+conversionTestCaseReader.getDescription());
		System.out.println("===================================");
		//loop multiple testcases to perform sequential testing
		for(ConversionTestCaseObject conversionTestCase:conversionTestCases) {
			String getResponse = "";
			String value = conversionTestCase.getInput().getValue();
			String from = conversionTestCase.getInput().getFrom();
			String to = conversionTestCase.getInput().getTo();

			System.out.println("Testing "+conversionTestCase.getDescription());
			System.out.println("Expecting output: "+conversionTestCase.getExpectedOutput());
			try {
				switch(conversionTestCaseReader.getDescription()) {
					case "length":
						getResponse = doGetRequest(lengthTest(value, from, to));
						break;
					case "weight":
						getResponse = doGetRequest(weightTest(value, from, to));
						break;
				}
				
			}catch(IOException ioEx) {
				System.out.println(ioEx.getMessage());
				ioEx.printStackTrace();
			}
			System.out.println(value+" :"+" from "+from+" to "+to+" = "+getResponse);
			System.out.println("-----------------------------------");
		}
	} 
	
	public void executeMathTests(TestCase mathTestCaseReader) {
		LinkedList<MathTestCaseObject> mathTestCases = mathTestCaseReader.getTestCaseObjectList();
		
		System.out.println("===================================");
		System.out.println("Testing "+mathTestCaseReader.getDescription());
		System.out.println("===================================");
		//loop multiple testcases to perform sequential testing
		for(MathTestCaseObject mathTestCase:mathTestCases) {
			String getResponse = "";
			String input = mathTestCase.getInput().getInput();
			String constant = mathTestCase.getInput().getConstant();
			
			String operator = "";
			
			System.out.println("Testing "+mathTestCase.getDescription());
			System.out.println("Expecting output: "+mathTestCase.getExpectedOutput());
			try {
				switch(mathTestCaseReader.getDescription()) {
					case "plus":
						getResponse = doGetRequest(plusTest(input, constant));
						operator = "+";
						break;
					case "minus":
						getResponse = doGetRequest(minusTest(input, constant));
						operator = "-";
						break;
					case "multiple":
						getResponse = doGetRequest(multipleTest(input, constant));
						operator = "*";
						break;
					case "divide":
						getResponse = doGetRequest(divideTest(input, constant));
						operator = "/";
						break;
				}
			} catch(IOException ioEx) {
				System.out.println(ioEx.getMessage());
				ioEx.printStackTrace();
			}
			System.out.println(input+" "+operator+" "+constant+" = "+getResponse);
			System.out.println("-----------------------------------");
		}
	}
	
	public static void main(String []args) throws IOException{
		
		TestURL example;
		try {
			example = new TestURL(args[0]);
		}
		catch (ArrayIndexOutOfBoundsException aioobEx) {
			System.out.println("No url provided, using default host localhost:8081");
			example = new TestURL();
		}
		
		//get input from external xml file with multiple inputs, this time taking length test cases input
		XmlConversionTestCaseReader xmlConversionTestCaseReader = new XmlConversionTestCaseReader();
		xmlConversionTestCaseReader.read(new File(args[1]));		
		example.executeConversionTests(xmlConversionTestCaseReader);
		
		//reuse the same conversion test case reader but take from another input file, weight test cases input
		xmlConversionTestCaseReader.read(new File(args[2]));
		example.executeConversionTests(xmlConversionTestCaseReader);	
		
		XmlMathTestCaseReader xmlMathTestCaseReader = new XmlMathTestCaseReader();
		xmlMathTestCaseReader.read(new File(args[3]));
		example.executeMathTests(xmlMathTestCaseReader);
		
		xmlMathTestCaseReader.read(new File(args[4]));
		example.executeMathTests(xmlMathTestCaseReader);
		
		xmlMathTestCaseReader.read(new File(args[5]));
		example.executeMathTests(xmlMathTestCaseReader);
		
		xmlMathTestCaseReader.read(new File(args[6]));
		example.executeMathTests(xmlMathTestCaseReader);
  }
}