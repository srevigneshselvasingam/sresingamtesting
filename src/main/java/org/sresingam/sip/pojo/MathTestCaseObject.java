package org.sresingam.sip.pojo;

public class MathTestCaseObject {
	
	private Input input;
	private String expectedOutput;
	private String description;
	
	public Input getInput() {
		return input;
	}
	public void setInput(Input input) {
		this.input = input;
	}
	public String getExpectedOutput() {
		return expectedOutput;
	}
	public void setExpectedOutput(String expectedOutput) {
		this.expectedOutput = expectedOutput;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public class Input{
		private String input;
		private String constant;
		public String getInput() {
			return input;
		}
		public void setInput(String input) {
			this.input = input;
		}
		public String getConstant() {
			return constant;
		}
		public void setConstant(String constant) {
			this.constant = constant;
		}
		
	}
}
