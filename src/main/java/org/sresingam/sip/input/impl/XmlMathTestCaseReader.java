package org.sresingam.sip.input.impl;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;

import org.sresingam.sip.input.TestCase;
import org.sresingam.sip.input.Reader;
import org.sresingam.sip.input.XmlReader;
import org.sresingam.sip.pojo.MathTestCaseObject;
import org.sresingam.sip.pojo.MathTestCaseObject.Input;

public class XmlMathTestCaseReader extends XmlReader implements Reader,TestCase {

	private LinkedList<MathTestCaseObject> mathTestCaseObjects;
	private String description;
	
	/**
	 * This function returns the description of the testcase
	 */
	@Override
	public String getDescription() {
		return description;
	}
	
	/**
	 * This function reads from a given File and retrieves test cases from the xml file. It then populate the testcases into a list for consumption by TestURL
	 * 
	 * @param file
	 * 				xml file consisting testcases
	 */
	@Override
	public void read(File file) {
		Document inputFile;
		Element math;
		List<Node> testCases;
		try {
			inputFile = parse(file);
			math = inputFile.getRootElement();
			if(!math.getName().equals("math")) {
				throw(new Exception("This is not math input file"));
			}
			this.description = math.selectSingleNode("/math").valueOf("@type");
			testCases = math.selectNodes("/math/testCase");
			mathTestCaseObjects = parseTestCases(testCases);
		}
		catch(Exception ex) {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		}
	}
	
	/**
	 * This function forms a LinkedList of testcases in the form of MathTestCaseObject. It converts a List of input in Node form into the said LinkedList 
	 * 
	 * @param testCases
	 * 				List of testCases in Node form
	 */
	private LinkedList<MathTestCaseObject> parseTestCases(List<Node> testCases){
		MathTestCaseObject mathTestCaseObject;
		mathTestCaseObjects = new LinkedList<MathTestCaseObject>();
		List<Node> testCaseList = testCases;
		for(Node testCase:testCaseList) {
			mathTestCaseObject = new MathTestCaseObject();
			
			String description = testCase.valueOf("@description");
			String expectedOutput = testCase.selectSingleNode("expectedOutput").getText();
			
			Input input = mathTestCaseObject.new Input();
			Node inputNode = testCase.selectSingleNode("input");
			input.setInput(inputNode.selectSingleNode("input").getText());
			input.setConstant(inputNode.selectSingleNode("constant").getText());
			
			mathTestCaseObject.setDescription(description);
			mathTestCaseObject.setExpectedOutput(expectedOutput);
			mathTestCaseObject.setInput(input);
			
			mathTestCaseObjects.add(mathTestCaseObject);
		}
		
		return mathTestCaseObjects;
		
	}

	/**
	 * This function returns the LinkedList of testcases
	 */
	@Override
	public LinkedList<MathTestCaseObject> getTestCaseObjectList() {
		// TODO Auto-generated method stub
		return mathTestCaseObjects;
	}


}
