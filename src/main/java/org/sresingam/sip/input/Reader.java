package org.sresingam.sip.input;

import java.io.File;
import java.util.LinkedList;

/** 
 * Reader interface. Implementation of this interface enable multiple way a 
 * file can be read. May it be streaming over connection or it could be read
 * from a local path. It could also can be implemented to read various type
 * of files, xml, json, excel.
 * @author Fazreil Amreen
 */
public interface Reader {

	/**
	 * read a given file
	 */
	public void read(File file);
	
}
