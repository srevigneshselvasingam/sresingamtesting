package org.sresingam.sip.input;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.dom4j.Document;
import org.dom4j.io.SAXReader;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XmlReader {
	
	protected Document parse(File xmlFile){
		Document xmlDocument = null;
		try {
			SAXReader reader = new SAXReader();
			xmlDocument = reader.read(xmlFile);
			xmlDocument.normalize();
		}
		catch(Exception ex) {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		}
		return xmlDocument;
		
	}
	
	protected String findValue(String tag, NodeList nodeList) {
		for(int i=0;i<nodeList.getLength();i++) {
			if(nodeList.item(i).getNodeName().equals(tag)) {
				return nodeList.item(i).getNodeValue();
			}
		}
		return null;
	}
	
	protected Node findNode(String tag, NodeList nodeList) {
		for(int i=0;i<nodeList.getLength();i++) {
			if(nodeList.item(i).getNodeName().equals(tag)) {
				return nodeList.item(i);
			}
		}
		return null;
	}

}
